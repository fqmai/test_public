package edu.stanford.cs108.cityinformation;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import java.lang.*;

public class AddActivity extends AppCompatActivity {

    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        setDb();
        setButtons();
    }

    private void setDb() {
        db = openOrCreateDatabase("CitiesDB",MODE_PRIVATE,null);
    }

    private void setButtons() {

        final Button btnSearch = (Button) findViewById(R.id.button_add);
        final EditText edtName = (EditText) findViewById(R.id.editText_name);
        final EditText edtContinent = (EditText) findViewById(R.id.editText_continent);
        final EditText edtPopulation = (EditText) findViewById(R.id.editText_population);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String addStr = "INSERT INTO cities VALUES('"
                + edtName.getText() + "', '" + edtContinent.getText()
                + "', " + edtPopulation.getText() + ", NULL);";
                Log.d("test", addStr);
                db.execSQL(addStr);
                Toast.makeText(getApplicationContext(), edtName.getText() + " Added",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}

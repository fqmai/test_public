package edu.stanford.cs108.cityinformation;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

public class LookupActivity extends AppCompatActivity {

    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lookup);
        setDb();
        setButtons();
    }

    private void setDb() {
        db = openOrCreateDatabase("CitiesDB",MODE_PRIVATE,null);
    }

    private void setButtons() {
        Button btnSearch = (Button) findViewById(R.id.button_search);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                EditText edtName = (EditText) findViewById(R.id.editText_name_lookup);
                EditText edtContinent = (EditText) findViewById(R.id.editText_continent_lookup);
                EditText edtPopulation = (EditText) findViewById(R.id.editText_population_lookup);
                RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
                RadioButton greater = (RadioButton) findViewById(R.id.radioButton_greater);
                RadioButton less = (RadioButton) findViewById(R.id.radioButton_less);

                String name_str = edtName.getText().toString();
                String continent_str = edtContinent.getText().toString();
                String population_str = edtPopulation.getText().toString();

                String selectStr = "SELECT * FROM cities"
                        + " WHERE city LIKE \"%"+ name_str + "%\""
                        + " AND continent LIKE \"%"+ continent_str + "%\"";
                int population_num = -1;
                if (population_str.length() != 0) {
                    population_num = Integer.parseInt(population_str);
                    if (greater.isChecked()) {
                        selectStr += " AND population > " + population_str + ";";
                    } else {
                        selectStr += " AND population < " + population_str + ";";
                    }
                }
                Log.d("test", selectStr);

                String[] fromArray = {"city", "continent", "population"};
                int[] toArray = {R.id.textView_city_name,
                        R.id.textView_continent_name, R.id.textView_city_population};
                Cursor cursor = db.rawQuery(selectStr, null);
                ListAdapter adapter = new SimpleCursorAdapter(
                        LookupActivity.this, R.layout.city_info, cursor, fromArray, toArray, 0);
                ListView listView = (ListView) findViewById(R.id.listView_cityInfo);
                listView.setAdapter(adapter);
            }
        });
    }
}


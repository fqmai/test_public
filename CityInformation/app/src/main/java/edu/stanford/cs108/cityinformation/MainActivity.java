package edu.stanford.cs108.cityinformation;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setButtons();
        setDatabase();
    }

    private void setDatabase() {
        db = openOrCreateDatabase("CitiesDB",MODE_PRIVATE,null);
        Cursor tablesCursor = db.rawQuery(
                "SELECT * FROM sqlite_master WHERE type='table' AND name='cities';", null);
        if (tablesCursor.getCount() == 0) {
            String setupStr = "CREATE TABLE cities ("
                    + "city TEXT, continent TEXT, population INTEGER,"
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT"
                    + ");";
            db.execSQL(setupStr);
            String initStr = "INSERT INTO cities VALUES(\"Cairo\",\"Africa\",15200000,NULL),"
                    + "(\"Lagos\",\"Africa\",21000000,NULL),"
                    + "(\"Kyoto\",\"Asia\",1474570,NULL),"
                    + "(\"Mumbai\",\"Asia\",20400000,NULL),"
                    + "(\"Shanghai\",\"Asia\",24152700,NULL),"
                    + "(\"Melbourne\",\"Australia\",3900000,NULL),"
                    + "(\"London\",\"Europe\",8580000,NULL),"
                    + "(\"Rome\",\"Europe\",2715000,NULL),"
                    + "(\"Rostov-on-Don\",\"Europe\",1052000,NULL),"
                    + "(\"San Francisco\",\"North America\",5780000,NULL),"
                    + "(\"San Jose\",\"North America\",7354555,NULL),"
                    + "(\"New York\",\"North America\",21295000,NULL),"
                    + "(\"Rio de Janeiro\",\"South America\",12280702,NULL),"
                    + "(\"Santiago\",\"South America\",5507282,NULL);";
            db.execSQL(initStr);
        }
    }

    private void setButtons() {

        Button btnLookupActivity = (Button)findViewById(R.id.button_lookup_activity);
        Button btnAddActivity = (Button)findViewById(R.id.button_add_activity);
        Button btnReset = (Button)findViewById(R.id.button_reset);

        btnLookupActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LookupActivity.class);
                startActivity(intent);
            }
        });

        btnAddActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.execSQL("DROP TABLE IF EXISTS cities;");
                setDatabase();
            }
        });
    }
}
